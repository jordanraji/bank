package service;

import java.sql.SQLException;

import repository.AccountRepository;
import domain.Account;

public class TransferService {

	private AccountRepository repository;
	private String message;

	public TransferService(AccountRepository repository) {
		this.repository = repository;
	}
	public String getMessage()
	{
		return message;
	}
	public boolean transfer(String sourceNumber, String targetNumber, double amount) throws SQLException {
		Account sourceAccount = repository.findByNumber(sourceNumber);
		if (sourceAccount == null) {
			message=("No se puede transferir por que no existe la primera cuenta");
			return false;
		}
		Account targetAccount = repository.findByNumber(targetNumber);
		if (targetAccount == null) {
			message=("No se puede transferir por que no existe la segunda cuenta");
			return false;
		}
		if (sourceAccount.getBalance() >= amount) {
			sourceAccount.setBalance(sourceAccount.getBalance() - amount);
			targetAccount.setBalance(targetAccount.getBalance() + amount);
			repository.save(sourceAccount);
			repository.save(targetAccount);
			return true;
		}
		message=("No se puede transferir por que la Cuenta 1 no tiene suficente monto para transferir.");
		return false;
	}
	public void close() throws SQLException{
		repository.close();
	}
}
