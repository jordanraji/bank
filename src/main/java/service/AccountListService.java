package service;

import java.sql.SQLException;
import java.util.List;

import repository.*;
import domain.Account;

public class AccountListService {

	AccountRepository repository;

	public AccountListService(AccountRepository repository) {
		this.repository = repository;
	}

	public List<Account> lista() throws SQLException
	{
		return repository.findAll();
	}
	public void close() throws SQLException{
		repository.close();
	}
}
