package controller;

import java.sql.SQLException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller

public class HomeController {
	@RequestMapping("/")
	String home(ModelMap model) throws SQLException {
		 return "home";
	}
}
