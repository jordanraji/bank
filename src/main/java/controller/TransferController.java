package controller;

import java.sql.SQLException;
import repository.*;
import service.TransferService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller

public class TransferController {
	@RequestMapping("/Accounts/Transfer")
	String home(ModelMap model) throws SQLException {
		 return "transfer";
	}
	@RequestMapping("/Accounts/TransferLOAD")
	String Trans(ModelMap model, @RequestParam String account1, @RequestParam String account2, @RequestParam String trans) throws SQLException {
		TransferService servicio=new TransferService(new InBDSQLITEAccountRepository());
		boolean noerror=servicio.transfer(account1,account2,Double.parseDouble(trans));
		model.addAttribute("error",!noerror);
		model.addAttribute("desc_error",servicio.getMessage());
		servicio.close();
		 return "transferLOAD";
	}
}
