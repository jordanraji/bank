package controller;

import service.AccountListService;
import java.sql.SQLException;
import repository.*;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
@Controller

public class AccountListController {
	@RequestMapping("/Accounts")
	String home(ModelMap model) throws SQLException {
		AccountListService serviciohome=new AccountListService(new InBDSQLITEAccountRepository());
		 model.addAttribute("acc",serviciohome.lista());
		 serviciohome.close();
		 return "accounts";
	}
}
