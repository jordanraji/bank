package repository;

import domain.Account;

import java.util.ArrayList;
import java.util.List;
import java.sql.*;

public class InBDSQLITEAccountRepository implements AccountRepository {
	Connection con = null;
    Statement st = null;
    ResultSet rs = null;
	public InBDSQLITEAccountRepository() throws SQLException {
		con = DriverManager.getConnection("jdbc:sqlite:bdproject.db");
		st=con.createStatement();
	}

	@Override
	public Account findByNumber(String number) throws SQLException {
		rs=st.executeQuery("SELECT * FROM Account where id_account="+number+";");
		if (rs.next())
		{
			return (new Account(rs.getString("id_account"),rs.getString("id_client"),Double.parseDouble(rs.getString("balance"))));
		}
		return null;
	}

	@Override
	public List<Account> findAll() throws SQLException {
		List<Account> list = new ArrayList<Account>();
		rs=st.executeQuery("SELECT * FROM Account;");
		while (rs.next())
		{
			list.add(new Account(rs.getString("id_account"),rs.getString("id_client"),Double.parseDouble(rs.getString("balance"))));
		}
		return list;
	}

	@Override
	public Account save(Account account) throws SQLException {
		if(account != null)
		{
			st.executeUpdate("UPDATE Account SET balance="+account.getBalance()+" WHERE id_account="+account.getNumber()+";");
			return account;
		}
		return null;
	}

	@Override
	public Account remove(Account account) {
		return null;
	}
	@Override
	public void close() throws SQLException{
		st.close();
		con.close();
	}
}