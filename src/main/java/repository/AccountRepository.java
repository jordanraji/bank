package repository;

import java.sql.SQLException;
import java.util.List;

import domain.Account;

public interface AccountRepository {
	Account findByNumber(String number) throws SQLException;

	List<Account> findAll() throws SQLException;

	Account save(Account account) throws SQLException;

	Account remove(Account account);
	
	void close() throws SQLException;
}
