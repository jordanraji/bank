package domain;

public class Account {
	private String number;
	private String client;
	private double balance;

	public Account() {
	}

	public Account(String number, String client,double balance) {
		this.number = number;
		this.balance = balance;
		this.client = client;
	}
	
	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public String getClient() {
		return client;
	}

	public void setClient(String client) {
		this.client = client;
	}
	
	
	public static Account copy(Account account) {
		if (account == null) {
			return null;
		}
		Account copy = new Account();
		copy.setBalance(account.getBalance());
		copy.setNumber(account.getNumber());
		copy.setClient(account.getClient());
		return copy;
	}
	
	@Override
	public String toString() {
		return "{number: " + number + ", balance: " + balance + "}";
	}
}
