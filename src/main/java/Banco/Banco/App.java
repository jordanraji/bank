package Banco.Banco;

import org.springframework.boot.autoconfigure.*;
import org.springframework.boot.SpringApplication;
import org.springframework.context.annotation.Import;
import config.WebConfig;

@Import(WebConfig.class)
@EnableAutoConfiguration

public class App 
{
    public static void main( String[] args )throws Exception
    {
    		SpringApplication.run(App.class,args);
    }
}